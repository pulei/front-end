import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router);

const Router1 =  new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    }
  ]
});

Router1.beforeEach(((to, from, next) => {
  console.log(to);
  console.log(from);
  console.log(next);
  next();
}));

export default Router1;
